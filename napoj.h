#ifndef NAPOJ_H
#define NAPOJ_H
#include<iostream>
#include"item.h"
using namespace std;

class Napoj : public Item
{
    public:
        Napoj(string nazov,int cena,int zivot);
        int getzivot();
    private:
        int m_zivot;
};

#endif // NAPOJ_H
