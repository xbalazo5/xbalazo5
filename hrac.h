#ifndef HRAC_H
#define HRAC_H
#include<vector>
#include<iostream>
#include"item.h"
#include"pirati.h"
using namespace std;

class Hrac
{
private:
   string m_meno;
   int m_zivot;
   int m_schopnost;
   vector<Item*> m_inventar;
   int m_peniaze;
    public:
        Hrac(string meno);
        void uberzivot(int kolko);
        void pridajzivot(int kolko);
        void pridajsurovinu(Item* co);
        void printInfo();
        void printInfoVeci();
        int getzivot();
        int getschopnost();
        int bojuj(Pirati*skym);
        int znizpeniaze(int kolko);
        int zvyspeniaze(int kolko);
        int zvysschopnost(int kolko);
        int getpeniaze();

};

#endif // HRAC_H
