#ifndef PLANETAOBCHOD_H
#define PLANETAOBCHOD_H
#include"planeta.h"
#include<iostream>
#include"item.h"
#include"zbran.h"
using namespace std;

class PlanetaObchod : public Planeta
{
    public:
        PlanetaObchod(string popis, Item* surovina);
        void integruj(Hrac* niekto);
        void printInfo();




    private:
        Item* m_surovina;
        Zbran* m_zbran;
};

#endif // PLANETAOBCHOD_H
