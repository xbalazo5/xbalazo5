#ifndef PLANETAPIRATI_H
#define PLANETAPIRATI_H
#include"planeta.h"
#include<iostream>
#include"hrac.h"
#include"pirati.h"
using namespace std;

class PlanetaPirati: public Planeta
{
    public:
        PlanetaPirati(string popis,Pirati* pirati);
        void integruj(Hrac* niekto);
        void printInfo();

    private:
        Pirati* m_pirati;
};

#endif // PLANETAPIRATI_H
