#ifndef PLANETA_H
#define PLANETA_H
#include<iostream>
#include"hrac.h"
using namespace std;

class Planeta
{
    public:
        Planeta(string popis);
        virtual void  integruj(Hrac* niekto)=0;
        virtual void  printInfo();
        string getpopis();

    private:
        string m_popis;
};

#endif // PLANETA_H
