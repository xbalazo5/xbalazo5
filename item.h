#ifndef ITEM_H
#define ITEM_H
#include<iostream>
using namespace std;

class Item
{
    public:
        Item(string nazov,int cena);
        string getnazov();
        int getcena();

    protected:

    private:
        string m_nazov;
        int m_cena;
};

#endif // ITEM_H
