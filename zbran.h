#ifndef ZBRAN_H
#define ZBRAN_H
#include"item.h"
#include<iostream>
using namespace std;


class Zbran : public Item
{
    public:
        Zbran(string nazov,int cena,int schopnost);
        int getschopnost();


    private:
        int m_schopnost;
};

#endif // ZBRAN_H
