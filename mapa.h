#ifndef MAPA_H
#define MAPA_H
#include"planeta.h"
#include"hrac.h"
#include<iostream>
using namespace std;


class Mapa
{
    public:
        Mapa();
        void pridajplanetu(Planeta* planetka);
        void odoberplanetu(Planeta*planetka);
        bool navstivplanetu(int ktoru);
        void printInfo();
        ~Mapa();
        Hrac* m_hrac;

    private:

        vector<Planeta*> m_planety;
        int m_pozicia;

};

#endif // MAPA_H
