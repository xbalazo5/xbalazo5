#ifndef PIRATI_H
#define PIRATI_H


class Pirati
{
    public:
        Pirati(int sila);
        int getsila();
    protected:

    private:
        int m_sila;
};

#endif // PIRATI_H
